File Structure:

|	Data - Contains all datafiles
	|	Analytics - Contains the output from analysis tools written to understand the datasets in more detail
	|	AndroidChatJson - Raw Android Chat Corpus
		|	Channel directories
			|	JSON files containg conversation data
	|	MachineLearningGeneratedData - Contains all machine learning generated data and data generated from
										the polar and nonpolar identification algorithms

	|	yqa_uk	- Contains all the data from Yahoo-based Contrastive Corpus of Questions and Answers
		|	Txt files named after the topic related to the content
	|	answerTrainingData.txt - answer training data used to train classifiers
	|	answerTrainingDataSmall.txt - small subset of answer training data used to test code before running on
										the larger set
	|	questionTrainingData.txt - question training data used to train classifiers
	|	answerTrainingDataSmall.txt - small subset of question training data used to test code before running on
										the larger set
	|	slack-1.1.10.zip - Original zip file sent from Android Chat Slack channel. Slack corpus zipped

|	PaperData - Data that was generated to put into the research paper
	| FinalTestData.txt - Full output from the final run of the program including metrics and test explanations
	| Other tests preceeding FinalTestData.txt

|	SlackBot - Contains ruby scripts for the slackbot originally built to read and save slack conversation data to MongoDB
				Not used after AndroidChat Slack corpus was obtained

|	src - Source code file for main program
	|	main.py - Main file that needs running
	|	lib - Contains classes
		|	questionAnswerNaiveBayes.py - Class implementation of the research project using Naive Bayes
		|	questionAnswerSVM.py - Class implementation of the research project using an SVM
|	tools - Data maniplution tools written in Ruby
	|	Analytics - Tools to generated greater understaning of data
		|	errorCounter.rb - Counts errors in machine learning data that was hand labeled after classification
		|	startWordCounter.rb - Counts the number of datapoints in a file
		|	stats.rb - Generated stastics on file numbers, utterances, directory names and more for Slack Corpus
	|	json - Contains tools to work with raw Slack Corpus data
		|	FileNames describe functions
	|	30mpqDataTasks.rb - Create dataset from the 30 million question corpus
	|	createTrainingData.rb - Generated training data

