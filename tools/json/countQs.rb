#!/usr/bin/env ruby

require "JSON"

Dir.chdir("../../Data/AndroidChatJson/labeledTestData")
fileList = Dir.glob("*.json")
errorFiles = []
questions = 0
File.open("./testthing.txt", 'w') do |k|
  fileList[0..21].each do |f|
    begin
      contents = File.open(f, "r"){ |file| file.read }
      hash = JSON.parse(contents)
      hash.each do |obj|
        object = obj["label"]
        object.each do |q|
          if q["type"] == "Q"
            k.write("__QuestionsStart__")
            k.write("\n#{obj["text"]}\n")
            k.write("__QuestionEnd__\n")
          end
        end
      end
    rescue
      errorFiles.push(f)
    end
  end
end

  puts questions

