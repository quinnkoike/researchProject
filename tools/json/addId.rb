#!/usr/bin/env ruby

require "JSON"

Dir.chdir("AndroidChatJson")
directoryList = []
Dir.glob("*").select do |d|
  directoryList.push(d) if File.directory?(d)
end

errorFiles = []

directoryList.each do |d|
  Dir.chdir(d)
  dnm = d[0...2]
  files = Dir.glob("*.json")
  id = 0
  files.each do |f|
    fnm = f[-7, 2]
    puts "Working on " + d.to_s + " : " + f.to_s
    begin
      contents = File.open(f, "r"){ |file| file.read }
      hash = JSON.parse(contents)
      hash.each do |obj|
        id += 1
        obj[:id] = fnm.to_s + dnm + id.to_s
        end
        File.open(f, "w+") do |writeF|
          puts hash
          writeF.write(JSON.pretty_generate(hash))
        end
      rescue
        errorFiles.push(f)
      end
      end
  Dir.chdir("..")
end

puts "Error files \n   | " + errorFiles.size.to_s + "\n   | "
puts errorFiles
