#!/usr/bin/env ruby

require "JSON"

Dir.chdir("AndroidChatJson")
directoryList = []
Dir.glob("*").select do |d|
  directoryList.push(d) if File.directory?(d)
end

total = directoryList.size
accumulator = 0
errorFiles = []

directoryList.each do |d|
  accumulator += 1
  puts "#{accumulator}/#{total}"
  Dir.chdir(d)
  files = Dir.glob("*.json")
  files.each do |f|
    begin
      contents = File.open(f, "r"){ |file| file.read }
      hash = JSON.parse(contents)
      hash.each do |obj|
        obj[:thread] = [{position: "null", number: 0}]
        # Type can be Q, A or null (Question, answer or null)
        obj[:label] = [{type: "null", position: "null"}]
        # Position can be S, E, C or null (start, end, continuation or null)
        end
        File.open(f, "w+") do |writeF|
          writeF.write(JSON.pretty_generate(hash))
        end
      rescue
        errorFiles.push(f)
      end
      end
  Dir.chdir("..")
end

puts "Error files \n   | " + errorFiles.size.to_s + "\n   | "
puts errorFiles
