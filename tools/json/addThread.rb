#!/usr/bin/env ruby

require "JSON"

Dir.chdir("AndroidChatJson")
directoryList = []
Dir.glob("*").select do |d|
  directoryList.push(d) if File.directory?(d)
end

errorFiles = []

directoryList.each do |d|
  Dir.chdir(d)
  files = Dir.glob("*.json")
  files.each do |f|
    begin
      contents = File.open(f, "r"){ |file| file.read }
      hash = JSON.parse(contents)
      hash.each do |obj|
        obj[:thread] = 0
        end
        File.open(f, "w+") do |writeF|
          writeF.write(JSON.pretty_generate(hash))
        end
      rescue
        errorFiles.push(f)
      end
      end
  Dir.chdir("..")
end

puts "Error files \n   | " + errorFiles.size.to_s + "\n   | "
puts errorFiles
