#!/usr/bin/env ruby

Dir.chdir("../Data/yqa_uk")
fileList = []
Dir.glob("*.txt").select do |f|
  fileList.push(f)
end

fileList.each do |doc|
  content = File.open(doc, 'r'){ |r| r.read }
  question = content.scan(/<question>([\s\S]*?)<\/question>/)
  File.open("../questionTrainingData.txt", 'a') do |l|
    question.each do |q|
      q.each do |a|
        l.puts "__QuestionStart__\n#{a.gsub(/^\r\n/, "").gsub(/\r\n$/, "")}\n__QuestionEnd__"
      end
    end
    File.open("../question-answers-2005.txt", "r").each_line do |line|
      l.puts "__QuestionStart__\n#{line.gsub(/^Q:|^Q: /, "")}__QuestionEnd__" if line.start_with?("Q:")
    end
    File.open("../question-answers-2006.txt", "r").each_line do |line|
      l.puts "__QuestionStart__\n#{line.gsub(/^Q:|^Q: /, "")}__QuestionEnd__" if line.start_with?("Q:")
    end
  end
end

fileList.each do |doc|
  content = File.open(doc, 'r'){ |r| r.read }
  answer = content.scan(/<answer>([\s\S]*?)<\/answer>/)
  File.open("../answerTrainingData.txt", 'a') do |l|
    answer.each do |a|
      a.each do |b|
        l.puts "__AnswerStart__\n#{b.gsub(/^\r\n/, "").gsub(/\r\n$/, "")}\n__AnswerEnd__"
      end
    end
    File.open("../question-answers-2005.txt", "r").each_line do |line|
      l.puts "__AnswerStart__\n#{line.gsub(/^Q:|^Q: /, "")}__AnswerEnd__" if  !line.start_with?("Q:")
    end
    File.open("../question-answers-2006.txt", "r").each_line do |line|
      l.puts "__AnswerStart__\n#{line.gsub(/^Q:|^Q: /, "")}__AnswerEnd__" if !line.start_with?("Q:")
    end
  end
end



qcount = 0
File.open("../questionTrainingData.txt", "r").each_line do |line|
  if line == "__QuestionStart__\n"
    qcount +=1
  end
end
acount = 0
File.open("../answerTrainingData.txt", "r").each_line do |line|
  if line == "__AnswerStart__\n"
    acount +=1
  end
end

puts "Training data contains #{qcount} questions"
puts "Training data contains #{acount} answers"
