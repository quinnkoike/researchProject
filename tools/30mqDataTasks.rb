#!/usr/bin/env ruby

Dir.chdir("../Data/MachineLearningGeneratedData")
fileList = "classifiedQuestionsM.txt"

# def stripErrors
# file = File.open("./classifiedQuestionsM.txt", 'rb')
# content = file.read
# f = content.gsub(/e__QuestionStart__\n.+\n__QuestionEnd__/, "")
# file.close
# file = File.open("./classifiedQuestionsM.txt", 'w')
# file.write(f)
# file.close
# end
 
def createDict(path)
  pureData = []
  copy = false
  string = ""
  File.open(path, 'r').each_line do |ln|
    if ln.strip() == "__QuestionStart__"
      copy = true
      string = " "
    elsif ln.strip() == "__QuestionEnd__"
      copy = false
      if string != " "
        pureData.push(string)
      end
    elsif copy
      string += " #{ln.strip()}"
    end
  end
  return pureData
end

questions = createDict("./#{fileList}")
  File.open("../questionTrainingData.txt", 'a') do |l|
      questions.each do |a|
        l.puts "__QuestionStart__\n#{a.gsub(/^\r\n/, "").gsub(/\r\n$/, "")}\n__QuestionEnd__"
    end
  end