require "JSON"
require "pp"
Dir.chdir("../../Data/AndroidChatJson")
directoryList = []
Dir.glob("*").select do |d|
  directoryList.push(d) if File.directory?(d)
end

errorFiles = []


count = 0
channelNames = []
fileHash = []
directoryList.each do |d|
  count += 1
  channelNames.push(d)
  Dir.chdir(d)
    files = Dir.glob("*.json")
    fileCount = files.size
    fileHash.push({d => fileCount})
  Dir.chdir("..")
end

errorFiles = []
objects = 0
directoryList.each do |d|
  Dir.chdir(d)
  dnm = d[0...2]
  files = Dir.glob("*.json")
  id = 0
  files.each do |f|
    begin
      contents = File.open(f, "r"){ |file| file.read }
      hash = JSON.parse(contents)
      hash.each do |obj|
        objects += 1
      end
      rescue
        errorFiles.push(f)
      end
      end
  Dir.chdir("..")
end

puts "Error files \n   | " + errorFiles.size.to_s + "\n   | "
puts errorFiles


puts("List of Channel Names:")
puts(channelNames)
puts("\nNumber of JSON files inside each channel:")
puts(fileHash)
puts("\nTotal number of Channels #{count}")

total = 0
fileHash.each do |h|
  h.each do |k,v|
    total += v
  end
end
puts("\nTotal number of JSON Files: #{total}")
puts("\nTotal number of utterances in dataset: #{objects}")

