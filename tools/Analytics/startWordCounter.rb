
puts "Enter File Location to count data points: "
path = gets.chomp
puts "Enter start word to search: "
startWord = gets.chomp()

dataPoints = 0

contents = File.open(path, "r").each_line do |l|
    a = l.split(" ")
    if startWord == a[0]
      dataPoints += 1
  end
end

puts "Data Points: #{dataPoints}"
File.open("#{Dir.pwd}/../../Data/Analytics/#{path.split('/').last}", 'w') do |f|
  f.write("#{path.split('/').last}: #{dataPoints}")
end
