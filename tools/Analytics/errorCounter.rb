
path = "/Users/quinn/LinguisticsResearch/Data/MachineLearningGeneratedData/classifiedQuestionsM.txt"
startWord = "__QuestionStart__"
errorWord = "e__QuestionStart__"

dataPoints = 0
errors = 0

contents = File.open(path, "r").each_line do |l|
    a = l.split(" ")
    if startWord == a[0]
      dataPoints += 1
    elsif errorWord == a[0]
    	errors += 1
  end
end

puts errors
puts dataPoints

File.open("#{Dir.pwd}/../../PaperData/firstExperiment.txt", 'w') do |f|
  f.write("Information on the first test of the general question classifier")
  f.write("#{errors + dataPoints} questions classified")
  f.write("\nFeatures used:")
  f.write("\n\tstartword\n\tembedded\n\tis\n\tare\n\twere\n\twas\n\tam\n\thas\n\thad\n\tdoes\n\tdid\n\twasn't\n\twasnt\n\tweren't\n\twerent\n\tshall\n\tshant\n\tshan't\n\twill\n\twon't\n\twont\n\twould\n\twouldn't\n\twouldnt\n\tshould\n\tshouldn't\n\tshouldnt\n\tcould\n\tcouldn't\n\tcouldnt\n\tisn't\n\tisnt\n\taren't\n\tarent\n\tdo\n\tdidn't\n\tdidnt\n\tdon't\n\tdont\n\tcan\n\tcan't\n\tcant\n\tmust\n\tmusn't\n\thave\n\thaven't\n\thavent\n\thasnt\n\thasn't\n\twhen\n\twhen's\n\twhens\n\twhere\n\twhere's\n\twheres\n\twhy\n\twhy's\n\twhys\n\twhys\n\twhy's\n\twhat\n\twhat's\n\twhats\n\twho\n\twho's\n\twhos\n\twhose\n\twhich\n\thow\n\thow's\n\thows\n\t?")
  f.write("\n#{((errors.to_f / (errors + dataPoints))* 100).round}% incorrect")
  f.write("\n#{((dataPoints.to_f / (errors + dataPoints))* 100).round}% correct")
end
