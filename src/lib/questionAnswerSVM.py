import nltk
import json
import random
import re
import os
from nltk.tokenize import word_tokenize
from sklearn.svm import LinearSVC
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support

class QuestionAnswerSVM:

    def __init__(self, qpath, apath, qstartWord, qendWord, astartWord, aendWord, testNum):
        self.qpath = qpath
        self.apath = apath
        self.qstartWord = qstartWord
        self.qendWord = qendWord
        self.astartWord = astartWord
        self.aendWord = aendWord
        self.features = []
        self.classifier = None
        self.questionHash = None
        self.answerHash = None
        self.test = testNum
        self.scores = None
        print("Creating question hash from dataset...")
        self.questionHash = self.createDict(qpath, qstartWord, qendWord)

        print("Creating non-question hash from dataset...")
        self.answerHash = self.createDict(apath, astartWord, aendWord)

        random.shuffle(self.questionHash)
        random.shuffle(self.answerHash)

        self.labelData(self.questionHash[:200000], self.answerHash[:200000])

        print("Running cross vailidation on 803000 questions and {} non-questions".format(len(self.answerHash)))
        self.scores = (self.crossValidate(10))
        print("Precision: {}%".format(self.scores[0][0]*100))
        print("Recall: {}%".format(self.scores[0][1]*100))
        print("F Score: {}%".format(self.scores[0][2]*100))
        print("Accuracy: {}%".format(self.scores[0][3]*100))

    def createDict(self, path, startWord, endWord):
        pureData = []
        #Solution from https://stackoverflow.com/questions/18865058/extract-values-between-two-strings-in-a-text-file-using-python
        with open(path) as f:
            copy = False
            for line in f:
                if self.validateString(line):
                    if line.strip() == startWord:
                        copy = True
                        string = " "
                    elif line.strip() == endWord:
                        copy = False
                        if string != " " :
                            pureData.append(string)
                    elif copy:
                        string += " " + line.strip()
            return pureData

    def addLabel(self, string, label):
        data = self.featureCreation(string)
        return (data, label)

    def featureCreation(self, string):
        questionWords = ["is", "are", "were", "was", "am", "has", "had", "does", "did", "wasn't", "wasnt", "weren't", "werent", "shall", "shant", "shan't", "will", "won't", "wont", "would", "wouldn't", "wouldnt", "should", "shouldn't", "shouldnt", "could","couldn't", "couldnt", "isn't", "isnt", "aren't", "arent", "do", "didn't", "didnt", "don't", "dont", "can", "can't", "cant", "must", "musn't", "have", "haven't", "havent", "hasnt", "hasn't", "when", "when's", "whens", "where", "where's", "wheres", "why", "why's", "whys", "whys", "why's", "what", "what's", "whats", "who", "who's", "whos", "whose", "which", "how", "how's", "hows", "?"]
        newString = self.preprocess(string)
    
        # Test 1 - Polar and Non Polar features, start words and question words    
        if self.test == 1:
            if not newString:
                singleFeature = {}
            else:
                singleFeature = {}
                for i in questionWords:
                    if i in newString:
                        singleFeature.update({i : 1})
                    else:
                        singleFeature.update({i : 0})

                if newString[0] in questionWords:
                    singleFeature.update({"startword": 1})
                else:
                    singleFeature.update({"startword": 0})

                if self.isNonPolar(newString):
                    singleFeature.update({"nonPolar": 1})
                else:
                    singleFeature.update({"nonPolar": 0})

                if self.isPolar(newString):
                    singleFeature.update({"polar": 1})
                else:
                    singleFeature.update({"polar": 0})

        # Test 2 - Question words and start words
        elif self.test == 2:
            if not newString:
                singleFeature = {}
            else:
                singleFeature = {}
                for i in questionWords:
                    if i in newString:
                        singleFeature.update({i : 1})
                    else:
                        singleFeature.update({i : 0})

                if newString[0] in questionWords:
                    singleFeature.update({"startword": 1})
                else:
                    singleFeature.update({"startword": 0})

        # Test 3 - Question words only
        elif self.test == 3:
            if not newString:
                singleFeature = {}
            else:
                singleFeature = {}
                for i in questionWords:
                    if i in newString:
                        singleFeature.update({i : 1})
                    else:
                        singleFeature.update({i : 0})

        # Test 4 - Polar and Non Polar features only
        elif self.test == 4:
            if not newString:
                singleFeature = {}
            else:
                singleFeature = {}
                if self.isNonPolar(newString):
                    singleFeature.update({"nonPolar": 1})
                else:
                    singleFeature.update({"nonPolar": 0})

                if self.isPolar(newString):
                    singleFeature.update({"polar": 1})
                else:
                    singleFeature.update({"polar": 0})
        else:
            print("Test selection out of bounds")

        return singleFeature

    def labelData(self, questionData, answerData):
        print ("\n\nLabelling Data")
        for j in answerData:
            self.features.append(self.addLabel(j, "NQ"))
        print("\tFinished labelling non-question data")
        for i in questionData:
            self.features.append(self.addLabel(i, "Q"))
        print("\tFinished labelling question data")

    def preprocess(self, string):
        a = string.lower().strip()
        tokenized = word_tokenize(a)
        return tokenized

    def buildQuestionList(self, directory):
        questionList = []
        errors = 0
        all_files = os.listdir(directory)
        for f in all_files:
            fi = open(directory + '/' + f)
            content = fi.read()
            try:
                j = json.loads(content)
                for i in j:
                    try:
                        ques = self.classifier.classify(self.featureCreation(i['text']))
                        if ques == 'Q':
                           questionList.append(i['text'])
                    except:
                        errors += 1
            except:
                errors += 1
        print(str(errors) + " errors were caught.")
        return questionList

    def validateString(self, string):
        regexp_join = re.compile(r'^<@([a-zA-Z0-9]+)[|a-z.\d\D\s\S]+> has joined the channel|^<@([a-zA-Z0-9]+)[|a-z.\d\D\s\S]+> set the channel')
        if any(word in string for word in ["http://", "https://"]) or bool(regexp_join.match(string)):
            return False
        else:
            return True

    def trainClassifier(self, trainData):
        self.classifier = SklearnClassifier(LinearSVC()).train(trainData)

    def predictLabels(self, data):
    	return self.classifier.classify_many(map(lambda t: t[0], data))

    def crossValidate(self, folds):
        random.shuffle(self.features)
        foldSize = len(self.features)//folds
        prec = 0
        rec = 0
        f = 0
        accuracy = 0
        results = []
        for i in range(folds):
            realLabels = []
            testData = self.features[i*foldSize:][:foldSize]
            trainingData = self.features[:i*foldSize] + self.features[(i+1)*foldSize:]
            for j in testData:
                (data, label) = j
                realLabels.append(label)
            self.trainClassifier(trainingData)
            totalValues = precision_recall_fscore_support(realLabels, self.predictLabels(testData), average='macro')
            prec += totalValues[0]
            rec += totalValues[1]
            f += totalValues[2]
            accuracy += accuracy_score(realLabels, self.predictLabels(testData))
        results.append((prec/folds, rec/(folds), f/folds, accuracy/folds))
        return results

    def isPolar(self, token):

        posttags = ["DT", "NNS", "NN", "NNP", "NNPS", "PRP", "PRP$", "VBG" "PDT", "RB", "RBR", "RBS", "IN", "TO"]
        howList = ["JJ", "RB"]
        nonPolarQuestionWords = ["when", "when's", "whens", "where", "where's", "wheres", "why", "why's", "whys", "whys", "why's", "what", "what's", "whats", "who", "who's", "whos", "whose", "which", "how", "how's", "hows"]
        polarQuestionWords = ["is", "are", "were", "was", "am", "has", "had", "does", "did", "wasn't", "wasnt", "weren't", "werent", "shall", "shant", "shan't", "will", "won't", "wont", "would", "wouldn't", "wouldnt", "should", "shouldn't", "shouldnt", "could","couldn't", "couldnt", "isn't", "isnt", "aren't", "arent", "do", "didn't", "didnt", "don't", "dont", "can", "can't", "cant", "must", "musn't", "have", "haven't", "havent", "hasnt", "hasn't"]

        pos = nltk.pos_tag(token)
        outcome = False
        how = False
        nonP = False
        if len(token) > 1:
            i = []
            for index, i in enumerate(pos):
                try:
                    if i[0].lower().strip() == "how" and pos[index+1][1] in howList:
                       how = True
                    elif index == 0 and i[0].lower().strip() in nonPolarQuestionWords:
                        nonP = True
                    elif i[0].lower() in polarQuestionWords:
                       try:
                           if index != 0:
                               if pos[index-1][0].lower().strip() != "how" and pos[index+1][1] in posttags:
                                   outcome  = True
                           elif index == 0 and pos[index+1][1] in posttags:
                               outcome = True
                       except IndexError:
                           pass
                except IndexError:
                   pass
            if how == True or nonP == True:
                outcome = False
        return outcome

    def isNonPolar(self, string):
        polarQuestionWords = ["is", "are", "were", "was", "am", "has", "had", "does", "did", "wasn't", "wasnt", "weren't", "werent", "shall", "shant", "shan't", "will", "won't", "wont", "would", "wouldn't", "wouldnt", "should", "shouldn't", "shouldnt", "could","couldn't", "couldnt", "isn't", "isnt", "aren't", "arent", "do", "didn't", "didnt", "don't", "dont", "can", "can't", "cant", "must", "musn't", "have", "haven't", "havent", "hasnt", "hasn't"]

        nonPolarQuestionWords = ["when", "when's", "whens", "where", "where's", "wheres", "why", "why's", "whys", "whys", "why's", "what", "what's", "whats", "who", "who's", "whos", "whose", "which", "how", "how's", "hows"]

        bigram = nltk.bigrams(string)
        outcome = False
        for i in list(bigram):
            if i[0].lower() in nonPolarQuestionWords and i[1].lower() in polarQuestionWords:
                outcome = True
        return outcome
