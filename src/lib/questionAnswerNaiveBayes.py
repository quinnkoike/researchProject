import nltk
import json
import random
import re
import os
from nltk.tokenize import word_tokenize
from sklearn.svm import LinearSVC
from nltk.classify.scikitlearn import SklearnClassifier

class QuestionAnswerNaiveBayes:

    def __init__(self, qpath, apath, qstartWord, qendWord, astartWord, aendWord, testNum):
        self.qpath = qpath
        self.apath = apath
        self.qstartWord = qstartWord
        self.qendWord = qendWord
        self.astartWord = astartWord
        self.aendWord = aendWord
        self.features = []
        self.train_set = []
        self.test_set = []
        self.confusion_matrix_data = []
        self.classifier = None
        self.questionHash = None
        self.answerHash = None
        self.test = testNum
        print("Creating question hash from dataset...")
        self.questionHash = self.createDict(qpath, qstartWord, qendWord)

        print("Creating non-question hash from dataset...")
        self.answerHash = self.createDict(apath, astartWord, aendWord)
        random.shuffle(self.questionHash)
        random.shuffle(self.answerHash)
        self.labelData(self.questionHash[:200000], self.answerHash[:200000])
        print("Training on 200,000 questions and {} non-questions".format(len(self.answerHash)))
        self.createTrainingAndTestSets()
        self.trainClassifier()

    def createDict(self, path, startWord, endWord):
        pureData = []
        #Solution from https://stackoverflow.com/questions/18865058/extract-values-between-two-strings-in-a-text-file-using-python
        with open(path) as f:
            copy = False
            for line in f:
                if self.validateString(line):
                    if line.strip() == startWord:
                        copy = True
                        string = " "
                    elif line.strip() == endWord:
                        copy = False
                        if string != " " :
                            pureData.append(string)
                    elif copy:
                        string += " " + line.strip()
            return pureData
            # End solution

    def labelData(self, questionData, answerData):
        print ("\n\nLabelling Data")
        for j in answerData:
            self.features.append(self.addLabel(j, "NQ"))
        print("\tFinished labelling non-question data")
        for i in questionData:
            self.features.append(self.addLabel(i, "Q"))
        print("\tFinished labelling question data")


    def addLabel(self, string, label):
        data = self.featureCreation(string)
        return (data, label)

    def featureCreation(self, string):
        questionWords = ["is", "are", "were", "was", "am", "has", "had", "does", "did", "wasn't", "wasnt", "weren't", "werent", "shall", "shant", "shan't", "will", "won't", "wont", "would", "wouldn't", "wouldnt", "should", "shouldn't", "shouldnt", "could","couldn't", "couldnt", "isn't", "isnt", "aren't", "arent", "do", "didn't", "didnt", "don't", "dont", "can", "can't", "cant", "must", "musn't", "have", "haven't", "havent", "hasnt", "hasn't", "when", "when's", "whens", "where", "where's", "wheres", "why", "why's", "whys", "whys", "why's", "what", "what's", "whats", "who", "who's", "whos", "whose", "which", "how", "how's", "hows", "?"]
        newString = self.preprocess(string)
        # An array containing all possible interogatives (question mark added to list of interogatives. Slight hack as a ? isn't an interogative, but better results are achived this way)
    
    # Test 1 - Polar and Non Polar features, start words and question words    
        if self.test == 1:
            if not newString:
                singleFeature = {}
            else:
                singleFeature = {}
                for i in questionWords:
                    if i in newString:
                        singleFeature.update({i : 1})
                    else:
                        singleFeature.update({i : 0})

                if newString[0] in questionWords:
                    singleFeature.update({"startword": 1})
                else:
                    singleFeature.update({"startword": 0})

                if self.isNonPolar(newString):
                    singleFeature.update({"nonPolar": 1})
                else:
                    singleFeature.update({"nonPolar": 0})

                if self.isPolar(newString):
                    singleFeature.update({"polar": 1})
                else:
                    singleFeature.update({"polar": 0})

# Test 2 - Question words and start words
        elif self.test == 2:
            if not newString:
                singleFeature = {}
            else:
                singleFeature = {}
                for i in questionWords:
                    if i in newString:
                        singleFeature.update({i : 1})
                    else:
                        singleFeature.update({i : 0})

                if newString[0] in questionWords:
                    singleFeature.update({"startword": 1})
                else:
                    singleFeature.update({"startword": 0})

# Test 3 - Question words only
        elif self.test == 3:
            if not newString:
                singleFeature = {}
            else:
                singleFeature = {}
                for i in questionWords:
                    if i in newString:
                        singleFeature.update({i : 1})
                    else:
                        singleFeature.update({i : 0})

# Test 4 - Polar and Non Polar features only
        elif self.test == 4:
            if not newString:
                singleFeature = {}
            else:
                singleFeature = {}
                if self.isNonPolar(newString):
                    singleFeature.update({"nonPolar": 1})
                else:
                    singleFeature.update({"nonPolar": 0})

                if self.isPolar(newString):
                    singleFeature.update({"polar": 1})
                else:
                    singleFeature.update({"polar": 0})
        else:
            print("Test selection out of bounds")

        return singleFeature

    def preprocess(self, string):
        a = string.lower().strip()
        tokenized = word_tokenize(a)
        return tokenized

    def createTrainingAndTestSets(self):
        print("Creating training set")
        print("Creating testing set")
        random.shuffle(self.features)
        featureSize = int((len(self.features)) / 2)

        self.train_set, self.test_set = self.features[:featureSize], self.features[featureSize:]

        print("Training set contains {}% of the total data".format((len(self.train_set)/len(self.features))*100))
        print("Testing set contains {}% of the total data".format((len(self.test_set)/len(self.features))*100))

    def trainClassifier(self):
        self.classifier = nltk.NaiveBayesClassifier.train(self.train_set)

    def getAccuracy(self):
        acc = nltk.classify.accuracy(self.classifier, self.test_set)
        return((acc * 100))

    def generate_confusion_matrix(self):
        TP, FP, TN, FN = 0,0,0,0
        for pair in self.train_set:
            (data, label) = pair
            prediction = self.classifier.classify(data)
            if prediction == "Q" and label == "Q":
                TP += 1
            elif prediction == "Q" and label == "NQ":
                FP += 1
            elif prediction == "NQ" and label == "Q":
                FN += 1
            elif prediction == "NQ" and label == "NQ":
                TN += 1

        total = TP + FP + FN + TN
        try:
            print("             Question     |      Answer")
            print("Question        {}                 {}".format(TP, FN))
            print("Non Question    {}                 {}".format(FP, TN))
            print("\nAccuracy: {}".format(((TP+TN)/total)*100))
            print("\nError Rate: {}".format(((FP+FN)/total)*100))
            print("\nRecall: {}".format((TP/(TP+FN))*100))
            print("Precision: {}".format((TP/(TP+FP))*100))
            print("Question Prevalence: {}".format(((TP+FN)/TP+FN)*100))
        except:
            pass

    def buildQuestionList(self, directory):
        questionList = []
        errors = 0
        all_files = os.listdir(directory)
        for f in all_files:
            fi = open(directory + '/' + f)
            content = fi.read()
            try:
                j = json.loads(content)
                for i in j:
                    try:
                        ques = self.classifier.classify(self.featureCreation(i['text']))
                        if ques == 'Q':
                            questionList.append(i['text'])
                    except:
                        errors += 1
            except:
                errors += 1
        print(str(errors) + " errors were caught.")
        return questionList

    def validateString(self, string):
        regexp_join = re.compile(r'^<@([a-zA-Z0-9]+)[|a-z.\d\D\s\S]+> has joined the channel|^<@([a-zA-Z0-9]+)[|a-z.\d\D\s\S]+> set the channel')
        if any(word in string for word in ["http://", "https://"]) or bool(regexp_join.match(string)):
            return False
        else:
            return True

    def isPolar(self, token):

        posttags = ["DT", "NNS", "NN", "NNP", "NNPS", "PRP", "PRP$", "VBG" "PDT", "RB", "RBR", "RBS", "IN", "TO"]
        howList = ["JJ", "RB"]
        nonPolarQuestionWords = ["when", "when's", "whens", "where", "where's", "wheres", "why", "why's", "whys", "whys", "why's", "what", "what's", "whats", "who", "who's", "whos", "whose", "which", "how", "how's", "hows"]
        polarQuestionWords = ["is", "are", "were", "was", "am", "has", "had", "does", "did", "wasn't", "wasnt", "weren't", "werent", "shall", "shant", "shan't", "will", "won't", "wont", "would", "wouldn't", "wouldnt", "should", "shouldn't", "shouldnt", "could","couldn't", "couldnt", "isn't", "isnt", "aren't", "arent", "do", "didn't", "didnt", "don't", "dont", "can", "can't", "cant", "must", "musn't", "have", "haven't", "havent", "hasnt", "hasn't"]

        pos = nltk.pos_tag(token)
        outcome = False
        how = False
        if len(token) > 1:
            i = []
            for index, i in enumerate(pos):
                try:
                   if i[0].lower().strip() == "how" and pos[index+1][1] in howList:
                       how = True
                   elif index == 0 and i[0].lower() in polarQuestionWords:
                       try:
                            if index != 0:
                               if pos[index-1][0].lower().strip() != "how" and pos[index+1][1] in posttags:
                                   outcome  = True
                            elif index == 0 and pos[index+1][1] in posttags and i[0].lower().strip() not in nonPolarQuestionWords:
                               outcome = True
                       except IndexError:
                           pass
                except IndexError:
                   pass
            if how == True:
                outcome = False
        return outcome

    def isNonPolar(self, string):
        polarQuestionWords = ["is", "are", "were", "was", "am", "has", "had", "does", "did", "wasn't", "wasnt", "weren't", "werent", "shall", "shant", "shan't", "will", "won't", "wont", "would", "wouldn't", "wouldnt", "should", "shouldn't", "shouldnt", "could","couldn't", "couldnt", "isn't", "isnt", "aren't", "arent", "do", "didn't", "didnt", "don't", "dont", "can", "can't", "cant", "must", "musn't", "have", "haven't", "havent", "hasnt", "hasn't"]

        nonPolarQuestionWords = ["when", "when's", "whens", "where", "where's", "wheres", "why", "why's", "whys", "whys", "why's", "what", "what's", "whats", "who", "who's", "whos", "whose", "which", "how", "how's", "hows"]

        bigram = nltk.bigrams(string)
        outcome = False
        for i in list(bigram):
            if i[0].lower() in nonPolarQuestionWords and i[1].lower() in polarQuestionWords:
                outcome = True
        return outcome
