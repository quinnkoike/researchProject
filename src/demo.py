from lib.questionAnswerNaiveBayes import QuestionAnswerNaiveBayes
from lib.questionAnswerSVM import QuestionAnswerSVM
from nltk.tokenize import word_tokenize
import os

def getRoot():
    return os.path.abspath('..')

def trainingDataQuestionPath():
    return "{}/Data/questionTrainingData.txt".format(getRoot())

def trainingDataNonQuestionPath():
    return "{}/Data/answerTrainingData.txt".format(getRoot())

test = QuestionAnswerNaiveBayes(trainingDataQuestionPath(), trainingDataNonQuestionPath(), "__QuestionStart__", "__QuestionEnd__", "__AnswerStart__", "__AnswerEnd__", 1)
print("Enter a question or 'Q 'to quit")
ques = ""
while ques != 'Q':
    ques = input("> ")
    a = test.classifier.classify(test.featureCreation(ques))
    print(a)
    if a == 'Q':
        print(test.isPolar(word_tokenize(ques)))


