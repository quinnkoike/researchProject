from lib.questionAnswerNaiveBayes import QuestionAnswerNaiveBayes
from lib.questionAnswerSVM import QuestionAnswerSVM
from nltk.tokenize import word_tokenize
import os

def getRoot():
    return os.path.abspath('..')

def trainingDataQuestionPath():
    return "{}/Data/questionTrainingData.txt".format(getRoot())

def trainingDataNonQuestionPath():
    return "{}/Data/answerTrainingData.txt".format(getRoot())


print("###################### NAIVE BAYES TEST START #######################")

for i in range(4):
    print("\n------------- TEST {} --------------\n\n".format(i+1))

    test = QuestionAnswerNaiveBayes(trainingDataQuestionPath(), trainingDataNonQuestionPath(), "__QuestionStart__", "__QuestionEnd__", "__AnswerStart__", "__AnswerEnd__", (i+1))

    print("\n\nTest {} - Accuracy level: {}%".format(i+1, test.getAccuracy()))
    test.generate_confusion_matrix()

    if i == 0:
        print("Training on start words, question words, polar and nonpolar")
    elif i == 1:
        print("Training on start words and question words")
    elif i == 2:
        print("Training question words")
    elif i == 3:
        print("Training on polar and nonpolar")

    print("\n\nTest {} - Most Likely Features: \n".format(i+1))
    print(test.classifier.show_most_informative_features(100))


    testQuestions = test.buildQuestionList("{}/Data/AndroidChatJson/social".format(getRoot()))

    f = open("{}/Data/MachineLearningGeneratedData/classifiedNBQuestions-Test{}.txt".format(getRoot(), i+1), 'w')
    print("\nWriting Test {} classified questions to {}/Data/MachineLearningGeneratedData/classifiedNBQuestions-Test{}.txt\n\n".format(i+1, getRoot(), i+1))
    for j in testQuestions:
        if test.validateString(j):
            f.write('__QuestionStart__\n')
            f.write(j)
            f.write('\n__QuestionEnd__\n')
    f.close()
    print("\n------------ TEST {} END ------------\n\n".format(i+1))


print("###################### NAIVE BAYES TEST END #######################")


print("###################### SVM TEST START #######################")

for i in range(4):
    print("\n------------- TEST {} --------------\n\n".format(i+1))

    test = QuestionAnswerSVM(trainingDataQuestionPath(), trainingDataNonQuestionPath(), "__QuestionStart__", "__QuestionEnd__", "__AnswerStart__", "__AnswerEnd__", (i+1))
    print('\n')
    if i == 0:
        print("Training on start words, question words, polar and nonpolar")
    elif i == 1:
        print("Training on start words and question words")
    elif i == 2:
        print("Training question words")
    elif i == 3:
        print("Training on polar and nonpolar")
    
    testQuestions = test.buildQuestionList("{}/Data/AndroidChatJson/troubleshooting".format(getRoot()))

    f = open("{}/Data/MachineLearningGeneratedData/classifiedSVMQuestions-Test{}.txt".format(getRoot(), i+1), 'w')
    print("\nWriting Test {} classified questions to {}/Data/MachineLearningGeneratedData/classifiedSVMQuestions-Test{}.txt\n\n".format(i+1, getRoot(), i+1))
    for j in testQuestions:
        if test.validateString(j):
            f.write('__QuestionStart__\n')
            f.write(j)
            f.write('\n__QuestionEnd__\n')
    f.close()
    print("\n------------ TEST {} END ------------\n\n".format(i+1))


f = open("{}/Data/MachineLearningGeneratedData/Polar.txt".format(getRoot()), 'w')
h = open("{}/Data/MachineLearningGeneratedData/NonPolar.txt".format(getRoot()), 'w')
print("\nWriting Polar Questions to {}/Data/MachineLearningGeneratedData/Polar.txt\n\n".format(getRoot()))
print("\nWriting Nonpolar Questions to {}/Data/MachineLearningGeneratedData/NonPolar.txt\n\n".format(getRoot()))
with open("{}/Data/MachineLearningGeneratedData/classifiedSVMQuestions-Test1.txt".format(getRoot())) as k:
    for line in k:
        if test.isPolar(word_tokenize(line)):
            f.write('__PolarQuestionStart__\n')
            f.write(line.strip())
            f.write('\n__PolarQuestionEnd__\n')
        elif test.validateString(line) and line.strip() != "__QuestionStart__" and line.strip() != "__QuestionEnd__":
            h.write('__NonPolarQuestionStart__\n')
            h.write(line.strip())
            h.write('\n__NonPolarQuestionEnd__\n')
h.close()
f.close()

labeledtestQuestions = test.buildQuestionList("{}/Data/AndroidChatJson/labeledTestData".format(getRoot()))

f = open("{}/Data/MachineLearningGeneratedData/classifiedSVMQuestions-Accuracy.txt".format(getRoot()), 'w')
print("\nWriting classified questions in hand labeled test to {}/Data/MachineLearningGeneratedData/classifiedSVMQuestions-Accuracy.txt\n\n".format(getRoot()))
for j in labeledtestQuestions:
    if test.validateString(j):
        f.write('__QuestionStart__\n')
        f.write(j)
        f.write('\n__QuestionEnd__\n')
f.close()
print("###################### PROGRAM END #######################")
